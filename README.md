# dpuptp

DPU PTP Magic - "It's about time" #dadjoke

This demo is based on the work that Itai Levy put togehter with this Rivermax DPU PTP Service container demo. This is the precursor to the DOCA 1.2 Firefly Container demo.

Itai's great work can be found here:

https://gitlab-master.nvidia.com/itailev/rivermax-dpu-ptp-service

This take on the above has two versions: one as a container, one as a native Linux command on the Bluefield-2. The Bluefield-2 will act as a GM for the Cumulus 4.4.1 switch.

### Prereqs

Run the following command before beginning:

```
ansible-galaxy install mrlesmithjr.netplan
```

The DPU must be in Separated Mode for this to work. See the DPU-PoC-Kit for setting up Separated Mode:

https://confluence.nvidia.com/display/NetworkingBU/SA+DPU+PoC+Kit

The Cumulus 4.4.1 configuration using NVUE can be found in the following directory:

```
switch_config/
```

There is also an Onyx configuration in there based on Itai's work.

### Setup a native PTP instance

1. Run the first playbook to prep the Bluefield-2(+)

```
ansible-playbook bf2_ptp_prep.yml
```

2. Run the following playbook to install and run the Linux PTP application as a background process on the Bluefield-2(+)

```
ansible-playbook bf2_ptp_native.yml
```

3. You can run the "ps aux | grep -i ptp" command to see the running processes on the Bluefield-2(+):

```
ubuntu@localhost:~$ ps aux | grep -i ptp
root        5990  0.0  0.0  12312  3756 pts/0    S    05:43   0:00 sudo /bin/sh -c /usr/sbin/ptp4l -i p0.51 -m -f  /etc/ptp4l/ptp4l.conf -m > /var/log/ptp4l.log
root        5992  0.0  0.0   2056   520 pts/0    S    05:43   0:00 /bin/sh -c /usr/sbin/ptp4l -i p0.51 -m -f  /etc/ptp4l/ptp4l.conf -m > /var/log/ptp4l.log
root        5993  0.0  0.0   2720   552 pts/0    S    05:43   0:00 /usr/sbin/ptp4l -i p0.51 -m -f /etc/ptp4l/ptp4l.conf -m
ubuntu      6124  0.0  0.0   7688   628 pts/1    S+   05:43   0:00 grep --color=auto -i ptp
```

Use the "sudo kill -9 <process ID>" to termainte the ptp4l application that is running in the background

### Setup as a container / WORK IN PROGRESS

1. Run the first playbook to prep the Bluefield-2(+)

```
ansible-playbook bf2_ptp_prep.yml
```

2. Run the following playbook to run the Linux PTP applicatino within a Docker container on the Bluefield-2(+)

```
ansible-playbook bf2_ptp_container.yml
```

### Troubleshooting

On the BF2, you should see the following in the logs:

```
sudo tail -f /var/log/ptp4l.log
ptp4l[1302.930]: selected /dev/ptp0 as PTP clock
ptp4l[1302.931]: port 1: INITIALIZING to LISTENING on INIT_COMPLETE
ptp4l[1302.932]: port 0: INITIALIZING to LISTENING on INIT_COMPLETE
ptp4l[1303.798]: port 1: LISTENING to MASTER on ANNOUNCE_RECEIPT_TIMEOUT_EXPIRES
ptp4l[1303.798]: selected local clock 043f72.fffe.d9fecc as best master
ptp4l[1303.798]: assuming the grand master role
```

The following address "043f72.fffe.d9fecc" is going to be what you see on the Cumulus switch as the reported Grandmaster. To see this on the Cumulus 4.4.1 switch, run the following command:

```
cumulus@sn2100:mgmt:~$ sudo pmc -u -b 1 -d 127 'GET TIME_STATUS_NP'
sending: GET TIME_STATUS_NP
	b8599f.fffe.5f217f-0 seq 0 RESPONSE MANAGEMENT TIME_STATUS_NP
		master_offset              0
		ingress_time               1633625637828079451
		cumulativeScaledRateOffset +0.000000000
		scaledLastGmPhaseChange    0
		gmTimeBaseIndicator        0
		lastGmPhaseChange          0x0000'0000000000000000.0000
		gmPresent                  true
		gmIdentity                 043f72.fffe.d9fecc
```

You will see the "gmPresent" = true if it is receiving PTP from the BF2. If not, the output will look like the following:

```
cumulus@sn2100:mgmt:~$ sudo pmc -u -b 1 -d 127 'GET TIME_STATUS_NP'
sending: GET TIME_STATUS_NP
	b8599f.fffe.5f217f-0 seq 0 RESPONSE MANAGEMENT TIME_STATUS_NP
		master_offset              0
		ingress_time               0
		cumulativeScaledRateOffset +0.000000000
		scaledLastGmPhaseChange    0
		gmTimeBaseIndicator        0
		lastGmPhaseChange          0x0000'0000000000000000.0000
		gmPresent                  false
		gmIdentity                 b8599f.fffe.5f217f
```

Run the following command to see the stream of PTP data coming from the DPU:

```
sudo tcpdump -i vlan51

listening on vlan51, link-type EN10MB (Ethernet), capture size 262144 bytes
16:49:22.472670 IP 172.20.0.11 > igmp.mcast.net: igmp v3 report, 2 group record(s)
16:49:23.251880 ARP, Request who-has 172.20.0.10 tell 172.20.0.1, length 28
16:49:23.317254 IP 172.20.0.11.ptp-general > ptp-primary.mcast.net.ptp-general: UDP, length 64
16:49:23.441303 IP 172.20.0.11.ptp-event > ptp-primary.mcast.net.ptp-event: UDP, length 44
16:49:23.441348 IP 172.20.0.11.ptp-general > ptp-primary.mcast.net.ptp-general: UDP, length 44
16:49:23.448587 IP 172.20.0.11 > igmp.mcast.net: igmp v3 report, 2 group record(s)
16:49:23.566295 IP 172.20.0.11.ptp-event > ptp-primary.mcast.net.ptp-event: UDP, length 44
16:49:23.566336 IP 172.20.0.11.ptp-general > ptp-primary.mcast.net.ptp-general: UDP, length 44
16:49:23.567207 IP 172.20.0.11.ptp-general > ptp-primary.mcast.net.ptp-general: UDP, length 64
```
